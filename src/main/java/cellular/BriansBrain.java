package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns){
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
    }
    
    @Override
    public void initializeCells() {
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
		for(int i = 0; i < currentGeneration.numRows(); i++){
			for(int j = 0; j < currentGeneration.numColumns(); j++){
				nextGeneration.set(i, j, getNextCell(i, j));
			}
		}
		currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState stateOfCell = currentGeneration.get(row, col);
		int neighborsAlive = countNeighbors(row, col, CellState.ALIVE);
		CellState nextStateOfCell = stateOfCell;
		if(stateOfCell == CellState.ALIVE){
            nextStateOfCell = CellState.DYING;
		}else if(stateOfCell == CellState.DYING){
			nextStateOfCell = CellState.DEAD;
		}else if(stateOfCell == CellState.DEAD && neighborsAlive == 2){
            nextStateOfCell = CellState.ALIVE;
        }
		return nextStateOfCell;
    }

    private int countNeighbors(int row, int col, CellState state) {
		int numNeighbors = 0;
		for(int i = row-1; i <= row+1; i++){
			if (i < 0 || i >= numberOfRows()) continue;
			for(int j = col-1; j <= col+1; j++){
				if (j < 0 || j >= numberOfColumns()) continue;
				if(!(i == row && j == col)){
					if(currentGeneration.get(i, j) == state){
						numNeighbors++;
					}
				}
			}
		}
		return numNeighbors;
	}


    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
    
}

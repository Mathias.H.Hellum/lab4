package datastructure;

import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {

    int columns;
    int rows;
    CellState initialState;
    CellState [][] grid;


    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.columns = columns;
        this.initialState = initialState;
        grid = new CellState[rows][columns];
        for (int row=0; row<rows; row++){
            for (int column=0; column<columns; column++){
                grid[row][column]=initialState;
            }
        }

	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return grid.length;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return grid[0].length;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if (row < 0 || row >= numRows()){
            throw new IndexOutOfBoundsException();
    }
        if (column < 0 || column>=numColumns()){
        throw new IndexOutOfBoundsException();
    }
        grid[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if (row < 0 || row >= numRows()){
            throw new IndexOutOfBoundsException();
        }
        if (column < 0 || column>=numColumns()){
            throw new IndexOutOfBoundsException();
        }    
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid grid_2= new CellGrid(numRows(), numColumns(), initialState);
        for (int row=0; row<rows; row++){
            for (int column=0; column<columns; column++){
                grid_2.set(row,column, get(row, column));
            }
        }
        return grid_2;
    }
    
}
